# ✨(Please add a title for your request)

/label ~feature ~needs-investigation
/cc @project-manager
/assign @scenic-dev

# Summary

(Summarize your feature request about Scenic, all this file is written in Markdown)

(Please submit this issue without the comments with parenthesis and remove the section that desn't concern your role)

## As a Scenic user I should be able to

(Please add something that could support and improve your usage of Scenic

### Usages?

- [ ] (Provide support for whatever protocol in the Scenic environment)
- [ ] (Add an alternative panel in Scenic in order to add new controlers)

### Why?

- [ ] (I want to support any kind of initiatives)

## As a Scenic maintainer I should be able to

(Please add something that would help us to support telepresence usages)

### Interests?

- [ ] (I developed a new feature in Switcher an I would like to control it with Scenic)
- [ ] (I made experimentation with some tools and I have ideas about its support)
- [ ] (I want to refactor something here because it will made my life better)

## Useful ressources

(Add all examples, guides, tools, specification or documentation you can about you request)

## Design suggestion

- [ ] (When Scenic is launched I should be able to see this button)
- [ ] (In this specific panel, I want to see this kind of information)
