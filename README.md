# Issue test

Test the `git subtree` in order to provide an independant set of templates for gitlab.

See https://gitlab.com/vlaurent/issue-test/-/issues/new?issuable_template=feature_request

## Steps

1. Initialize the distant repository as remote (only required for new repository)

```bash
git remote add -f gitlab-config https://gitlab.com/sat-mtl/telepresence/issue-templates.git
```

2. Fetch the content of the distant repository as a subtree

```bash
git subtree add --prefix .gitlab gitlab-config main --squash
```

3. Update the content of the distant repository into the local subtree

```bash
git subtree pull --prefix .gitlab gitlab-config main --squash
```

4. Don't forget to commit and push the local changes to the origin

## References

- [Altasian guide on `git subtree`](https://www.atlassian.com/git/tutorials/git-subtree)
- [A git `submodule` to `subtree` converter](https://stackoverflow.com/questions/28215244/convert-git-submodule-to-subtree)
